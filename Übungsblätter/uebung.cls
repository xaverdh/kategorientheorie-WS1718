\ProvidesClass{uebung}

\RequirePackage{ifthen}

% language switching setup
\newif\ifde
\newif\ifen
\ifthenelse{\equal{\sprache}{de}}{\detrue}{}
\ifthenelse{\equal{\sprache}{en}}{\entrue}{}
\newcommand{\de}[1]{\ifde#1\fi}
\newcommand{\en}[1]{\ifen#1\fi}
\newcommand{\deen}[2]{\de{#1}\en{#2}}

\ifde
  \LoadClass[a4paper, ngerman]{scrartcl}
\fi
\ifen
  \LoadClass[a4paper]{scrartcl}
\fi

\RequirePackage[utf8]{inputenc}
\ifde
  \RequirePackage[ngerman]{babel}
\fi
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{enumitem}
\RequirePackage{mathtools}
\RequirePackage{geometry}
\RequirePackage{csquotes}


\geometry{top=2.5cm}
\setlength{\parindent}{0pt}
\setlength{\parskip}{\medskipamount}


\renewcommand{\maketitle}[2]{
  \pagestyle{empty}
  Universität Augsburg \hfill
  Prof.~Dr.~Marc Nieper-Wißkirchen \\
  Lehrstuhl für Algebra und Zahlentheorie \hfill
  Matthias Hutzler, B. Sc. \\
  Wintersemester 2017/2018 \\[0em]

  \begin{center}
  \Large
  \textbf{%
    \de{Übungsblatt #1 zur Kategorientheorie}%
    \en{Exercise Sheet #1 for Category Theory}} \\[1em]
  \end{center}
\vspace{1.5em}}

\newcounter{aufgabennummer}
\newenvironment{aufgabe}[2][]{
  \addtocounter{aufgabennummer}{1}
  \textbf{\de{Aufgabe}\en{Exercise} \theaufgabennummer.}%
  \ifstrempty{#1}{}{ (#1)} \emph{#2} \par}
  {\vspace{1.5em}}

\newenvironment{teilaufgaben}
  {\begin{enumerate}[label=(\alph*)]}{\end{enumerate}}


\newcommand{\NN}{\mathbb{N}}
\newcommand{\defeq}{\vcentcolon=}
\newcommand{\hole}{\mathord{\,\cdot\,}}  % hole for an argument
\renewcommand{\SS}{\mathbb{S}}  % why is \SS already defined?
\renewcommand{\P}{\mathcal{P}}
\DeclareMathOperator{\dom}{dom}
\DeclareMathOperator{\cod}{cod}
\newcommand{\id}{\mathrm{id}}
\DeclareMathOperator{\Ob}{Ob}
\DeclareMathOperator{\Mor}{Mor}
\newcommand{\To}{\Rightarrow}
\newcommand{\Id}{\mathrm{Id}}
\newcommand{\Hom}{\mathrm{Hom}}
\newcommand{\Nat}{\mathrm{Nat}}
\newcommand{\op}[1]{{#1}^o}
% global category symbols
\newcommand{\category}[1]{\mathrm{#1}}
\newcommand{\Set}{\category{Set}}
\newcommand{\Vect}{\category{Vect}}
\newcommand{\Grp}{\category{Grp}}
\newcommand{\Ab}{\category{Ab}}
\newcommand{\CRing}{\category{CRing}}
\newcommand{\Cat}{\category{Cat}}
\newcommand{\sSet}{\category{sSet}}
